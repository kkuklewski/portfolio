import React from 'react';
import {Container} from "reactstrap";
import man from "../../images/spaceman/man.svg"

const BigEntry = () => {
  return (
   <Container fluid className="bigEntry">
      <Container fluid className="bigEntry-section">
         <>
            <div className="entryCol">
               <aside>
                  <p>
                     <img id="spaceMan" className="spaceMan" style={{width:'100px'}} src={man} alt=""/>
                     <div>
                        <span
                          className="spanMain"
                          data-aos-delay="200"
                          data-aos="fade-up">I'm problem solver</span>
                        <span
                          className="spanMain"
                          data-aos-delay="700"
                          data-aos="fade-up">quick studier, self starter</span>
                        <span                        className="spanMain" data-aos="fade-in">& communicator with a passion for great design</span>
                        <span                        className="spanMain" data-aos-offset="100" data-aos-delay="1400" data-aos="fade-up">and the ability to</span> <span data-aos-offset="100" data-aos-delay="1200" data-aos="fade-up" className="code-text black-white">achieve it.</span>
                     </div>
                  </p>
               </aside>
            </div>
         </>
      </Container>
   </Container>
  );
};

export default BigEntry;