import React from 'react';
import {useState} from "react";
import {Container,Row,Col,Button,Modal, ModalHeader, ModalBody, ModalFooter,Progress,Badge} from "reactstrap";
import records from "./records.js"

function Portfolio () {

   const [modal, setModal] = useState({open: null});

   const toggle = (param) => setModal({open: param});

   return (
     <Container className="portfolioContainer">
        <Row>
           <Col className="my-5" md={{size:9,offset:2}}>
              <h3 className="code-text" data-aos="flip-right" data-aos-delay="500">Latest Projects</h3>
           </Col>
           <Col md={{size:10,offset:2}} className="d-flex flex-wrap gap-4">
              {
                 records && records.map( record => {
                    return (
                      <Col onClick={() => toggle(record.id)} key={record.id} md="5" lg="5" xl="3" xxl="3" className="bg-white flex-column rounded-4 overflow-hidden shadow-sm">
                         {
                            record.video
                              ? <video className="img-fluid cardPortfolio card-img" autoPlay={true} loop={true} muted={true} src={record.video}/>
                              : <img src={record.image} className="img-fluid card-img cardPortfolio" alt=""/>
                         }
                         <div className="my-2 p-3 card-header">
                            <Badge color={record.meta.color}>
                               {record.meta.tags}
                            </Badge>
                            <h3 className="accordion-header">{record.title}</h3>
                            <p className="text-muted">{record.short}</p>
                            {/*<Button block className="mt-3" onClick={() => toggle(record.id)}>Open</Button>*/}
                         </div>
                         <Modal className="modalComponent" style={{margin:'0 auto'}} isOpen={modal.open === record.id} onClosed={() => toggle(null)}>
                            <div>
                               <ModalHeader>{record.title} [{modal.open}]</ModalHeader>
                               {/* Modal Body*/}
                               <ModalBody>
                                  <Col>
                                     <img src={record.image} className="img-fluid card-img cardPortfolio" alt=""/>
                                  </Col>
                                  <Col>
                                     <h1 className="mb-2 mt-4">{record.title}</h1>
                                     <p style={{fontSize:'1.25rem'}}>{record.long}</p>
                                     {
                                        record.body
                                       ? <p>{record.body}</p>
                                          : null
                                     }
                                  </Col>
                                  <div>
                                     <Row className="skillsTab">
                                        <Col className="my-3">
                                           <h4 className="card-header mb-3 text-muted">                    Used Skills</h4>
                                           {
                                              (record.meta.skills || []).map(skill => (
                                                <div className="my-4" key={skill.name}>
                                                   <h5>{skill.name}</h5>
                                                   <Progress
                                                     color={skill.status}
                                                     value={skill.number}
                                                   />
                                                </div>
                                              ))
                                           }
                                        </Col>
                                     </Row>
                                  </div>
                               </ModalBody>
                               {/* Footer */}
                               <ModalFooter>

                                  {
                                     record.url
                                       ? <a target="_blank" href={record.url}><Button size="lg" color="primary" onClick={() => toggle(null)}>Url</Button></a>
                                       : null
                                  }
                                 {' '}
                                  <Button size="lg" color="secondary" onClick={() => toggle(null)}>
                                     Go Back
                                  </Button>
                               </ModalFooter>
                            </div>
                         </Modal>
                      </Col>
                    )
                 })
              }
           </Col>
        </Row>
     </Container>
   );
}

export default Portfolio;