import hero from '../../images/hero.jpg';
import freshlab from "../../video/freshlab.mp4"
import szpaku from "../../video/szpakutour.mp4"
import szpakuThumb from "../../images/thumbs/szpaku.jpg"
import freshThumb from "../../images/thumbs/fresh-thum.jpg"
import hiphopThumb from "../../images/thumbs/hhszn.jpg"
import Lux from "../../images/thumbs/luxonis.jpg"
import usugi from "../../images/thumbs/usugi-2.jpg"

export default [
  {
    "id" : 1,
    "title" : "Freshlab",
    "short" : "E-Commerce Web App",
    "long" : "Freshlab is full e-commerce green Web App that helps people easily order healthy food to their houses. The app contains many photos, videos and animated GIFs. In this project, i was response for Design, frontend code using React JS but also deliver high end video",
    "shorttext" : "Skrót projektu",
    "url" : "https://freshlab.pl",
    "video": freshlab,
    "image": freshThumb,
    "meta": {
      "color": "primary",
      "tags" : "frontend",
      "skills": [
        {"name": "Figma", "number": "65", "status" : "warning"},
        {"name": "React", "number": "35", "status" : "primary"},
        {"name": "After Effects", "number": "15", "status" : "primary"},
        {"name": "Antd", "number": "25", "status" : "primary"},
        {"name": "Reactstrap", "number": "25", "status" : "primary"},
      ],
    }
  },
  {
    "id" : 2,
    "title" : "Luxonis",
    "short" : "Landing Page",
    "long" : "Meet Luxonis spatial AI and CV embedded machine. Responsible to create unique design of Landing Page. I prepared also high resolutions photos and 3d render's using Blender software",
    "image": Lux,
    "meta": {
      "color": "warning",
      "tags" : "UI/UX",
      "skills": [
        {"name": "Figma", "number": "100", "status" : "warning"},
        {"name": "Adobe Photoshop", "number": "70", "status" : "primary"},
        {"name": "Blender", "number": "35", "status" : "primary"},
        {"name": "DavinciResolve", "number": "65", "status" : "primary"},
      ],
    }
  },
  {
    "id" : 3,
    "title" : "Usugi",
    "short" : "Web App & Mobile Version",
    "long" : "Usugi is a platform for buying and managing services. In this project, i was responsive to creating fully Web App design based on client delivered colour scheme",
    "image": "https://cdn.dribbble.com/users/6958183/screenshots/18536022/media/216b545c563145c0f886d4ed94b8ca46.png?compress=1&resize=1600x1200&vertical=top",
    "thumb": usugi,
    "meta": {
      "color": "warning",
      "tags" : "UI/UX",
      "skills": [
        {"name": "Figma", "number": "100", "status" : "warning"},
      ],
    }
  },
  {
    "id" : 4,
    "title" : "beginKeep",
    "short" : "Login Page Component",
    "long" : "Login Page Component i have been responsive to create fully design with color scheme",
    "image": "https://cdn.dribbble.com/users/6958183/screenshots/18259269/media/40c007596162f750073e8da0c13157a8.jpg?compress=1&resize=1600x1200&vertical=top",
    "meta": {
      "color": "warning",
      "tags" : "UI/UX",
      "skills": [
        {"name": "Figma", "number": "100", "status" : "warning"},
        {"name": "Affinity Designer", "number": "30", "status" : "primary"},
      ],
    }
  },
  {
    "id" : 5,
    "title" : "Ticketos",
    "short" : "Landing Page",
    "long" : "Created prototype of new mobile App for one of the biggest event resseler. It's clean, modern, and minimal style",
    "image" : "https://cdn.dribbble.com/users/6958183/screenshots/18259433/media/1de676ab4a28ed195320a65a4cd31dc1.png?compress=1&resize=1600x1200&vertical=top",
    "meta": {
      "color": "warning",
      "tags" : "ui/ux",
      "skills": [
        {"name": "Figma", "number": "100", "status" : "primary"},
      ],
    }
  },
  {
    "id" : 6,
    "title" : "DDZPPM Tour",
    "short" : "Landing Page",
    "url" : "https://ddzppmtour.pl",
    "image" : szpakuThumb,
    "video": szpaku,
    "meta": {
      "color": "primary",
      "tags" : "frontend",
      "skills": [
        {"name": "HTML", "number": "100", "status" : "warning"},
        {"name": "Bootstrap", "number": "50", "status" : "primary"},
        {"name": "SaaS", "number": "50", "status" : "primary"},
      ],
    }
  },
  {
    "id" : 7,
    "title" : "HipHopSzczecin",
    "short" : "Landing Page",
    "url" : "https://hiphopszczecin.pl",
    "image" : hiphopThumb,
    "video" : "https://hiphopszczecin.pl/video/elipsa_obrot2.mp4",
    "meta": {
      "color": "primary",
      "tags" : "frontend",
      "skills": [
        {"name": "HTML", "number": "100", "status" : "warning"},
        {"name": "Bootstrap", "number": "100", "status" : "primary"},
        {"name": "SaaS", "number": "50", "status" : "primary"},
      ],
    }
  },
  {
    "id" : 8,
    "title" : "Crypto Kantor",
    "short" : "Mobile App",
    "long" : "Design of an mobile app to easily assets of cryptocurrencies. Created from scratch to a fully prototype with color scheme and branding idea",
    "image": "https://cdn.dribbble.com/users/6958183/screenshots/18535954/media/c537fe81eeaa0087c35d1c96388989a0.png?compress=1&resize=1200x900&vertical=top",
    "meta": {
      "color": "warning",
      "tags" : "UI/UX",
      "skills": [
        {"name": "Figma", "number": "100", "status" : "warning"},
        {"name": "Affinity Designer", "number": "45", "status" : "primary"},
      ],
    }
  },
]