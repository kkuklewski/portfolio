import React from 'react';
import { Link } from "react-router-dom";
import { Container, Row, Col} from "reactstrap";
import logo from "../images/kamil-logo.svg";

const NavigationBar = () => {
   return (
      <Container fluid className="hero">
         <Container fluid="lg" className="navigationBar">
            <Row className="p-4">
               <Col className="d-flex justify-content-around align-items-center">
                  <Link to="/">
                     <img className="img-fluid" style={{maxWidth:'100px',filter:'invert(100)'}} src={logo} alt=""/></Link>
                  <div className="navbar">
                     {/*<Link to="/">home</Link>*/}
                     {/*<Link to="/about">about</Link>*/}
                     {/*<Link to="/projects">projects</Link>*/}
                     {/*<Link to="/contact">contact</Link>*/}
                     {/*<Button size="lg" color="primary">Kontakt</Button>*/}
                     <a className="mainButton" target="_blank" href="https://gitlab.com/kkuklewski/portfolio"><span style={{color:'white',fontSize:'14px', marginRight:'10px', fontWeight:'100'}}>view on</span><img style={{maxWidth:'35px', filter:'invert(100)'}} className="img-fluid" src="https://cdn-icons-png.flaticon.com/512/5968/5968813.png" alt=""/></a>
                  </div>
               </Col>
            </Row>
         </Container>
      </Container>
   );
};

export default NavigationBar;