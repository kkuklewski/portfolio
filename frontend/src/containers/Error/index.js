import React from 'react';
import {Container,Row,Col} from "reactstrap";


const Error = () => {
  return (
   <Container fluid>
      <Row>
         <Col data-aos="fade-up" lg={{size:'6',offset:'3'}} className="border-5" style={{backgroundColor:'white',borderRadius:'16px'}}>
            <div style={{padding:'1rem',textAlign:'center'}}>
               <h2 style={{color:'orange'}}>.. Sorry!</h2>
               <h4>
                  This site is not yet preparied, but i'm doing my best.
               </h4>
            </div>
            <img className="img-fluid" src="https://img.freepik.com/free-vector/oops-404-error-with-broken-robot-concept-illustration_114360-5529.jpg?w=826&t=st=1664790927~exp=1664791527~hmac=1b9c5270532bfd6dc710a30313e4a71cfcba6fe5ef2c2176dacbcd6fde0ffd17" alt=""/>
         </Col>
      </Row>
   </Container>
  );
};

export default Error;