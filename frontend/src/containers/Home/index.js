import React from 'react';
import {Container,Row,Col,Button,Progress} from "reactstrap";
import Portfolio from "../../components/Portfolio";
import aboutIMG from "../../images/roadmap.png"
import BigEntry from "../../components/BigEntry";
import profilePhoto from "../../images/profile-photo.jpg"

// images
import rocket  from "../../images/rocket/rocket.png"
import clouds  from "../../images/rocket/clouds.png"
import fire  from "../../images/rocket/fire.png"
import moon  from "../../images/rocket/moon.png"
import mountains  from "../../images/rocket/mountains.png"
import stars  from "../../images/rocket/stars.png"


const Skills = () => {
   return (
     <Row className="skillsTab">
        <div>
           <h5>Filmmaker</h5>
           <Progress
             animated
             color="success"
             value={80}
           />
        </div>
        <div>
           <h5>Video Editor</h5>
           <Progress
             animated
             color="secondary"
             value={100}
           />
        </div>
        <div>
           <h5>Self-taught Front-end Developer </h5>
           <Progress
             animated
             color="primary"
             value={45}
           />
        </div>
        <div>
           <h5>UI/UX Developer</h5>
           <Progress
             animated
             color="secondary"
             value={85}
           />
        </div>
     </Row>
   )
}

const Clients = () => {

   const widthFive = {
      maxWidth:'25px',
   }

   return (
     <div className="d-flex clientRow">
        <img className="img-fluid"  src="https://ticketos.pl/static/images/ticketos_logo.png" alt=""/>
        <img className="img-fluid"  src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwszczecinie.pl%2Fthumb%2Farticles%2F848-1370960602.jpg&f=1&nofb=1&ipt=ad883ab2723a130663503a234fbaff2980d3f7f60943bc44db77ba296d18d283&ipo=images" alt=""/>
        <img className="img-fluid"  src="https://mui.com/static/branding/companies/nasa-light.svg" alt=""/>
     </div>
     )
}

const Hero = () => {
   return (
     <Container className="contact" fluid>
        <Container>
           <Row className="justify-content-center hero-row">
              <Col sm={{size:4,offset:1}} md={{size:4,offset:1}} className="mobile-none">
                 <img data-aos="fade-in" className="img-fluid imageContainer imageDrop" src={profilePhoto} alt=""/>
              </Col>
              <Col md={{size:6,offset:1}} lg={{size:4,offset:0}}>
                 <div className="text-box" style={{padingBottom:'100px'}}>
                    <div data-aos="fade-left">
                       <h4 className="text-muted code-text">Visual Designer</h4>
                       <h3>Kamil Kuklewski</h3>
                    </div>
                    <p className="text-muted" style={{fontSize:'2rem'}} data-aos="fade-in">I’ve been doing filmmaking for the last 8 years, both as a director, production manager, filmmaker, video editor.</p>
                    <p data-aos="fade-left">Projects we created together were ranging from short social-media clips to multi-team full-length productions which I directed.</p>
                    <p data-aos="fade-right">Making a next step in helping my clients grow on the internet, I started to use modern web technologies to build great looking websites and applications</p>
                 </div>
                 <div className="contactBox">
                    <a href="mailto:kontakt@a1v.pl"><Button data-aos="flip-up" size="lg" className="mt-4" block color="secondary">E-mail</Button></a>
                    <a href="tel:570295900"><Button data-aos="flip-up" size="lg" className="mt-4" block color="primary">Phone</Button></a>
                 </div>
                 <div className="socialBox flex-row" data-aos="fade-down">
                    <h5 className="text-muted">Read more:</h5>
                    {/* contact icons */}
                    <a href="https://www.linkedin.com/in/kamil-kuklewski/" target="_blank"><img className="img-fluid" src="https://cdn-icons-png.flaticon.com/512/3536/3536505.png" alt=""/></a>
                    <a href="https://gitlab.com/kkuklewski/portfolio" target="_blank"><img className="img-fluid" src="https://cdn-icons-png.flaticon.com/512/5968/5968853.png" alt=""/></a>
                    <a href="https://www.instagram.com/a1vstudio/" target="_blank">                    <img className="img-fluid" src="https://cdn-icons-png.flaticon.com/512/2111/2111463.png" alt=""/></a>
                 </div>
              </Col>
           </Row>
        </Container>
     </Container>
   );
};

const About = () => {
   return (
      <Container>
         <Row className="align-items-center">
            <Col className="mobile-none" md={{size:3,offset:2}}>
               <img className="img-fluid" src={aboutIMG} alt=""/>
            </Col>
            <Col className="aboutBox" md={{size:4, offset:1}} lg={{size:3, offset:1}}>
               <h3 className="code-text">01</h3>
               <h2>The Story</h2>

            </Col>
         </Row>
         <div className="skewed mobile-none"></div>
      </Container>
   )
}


class HeroRocket extends React.Component {
   constructor(props) {
      super(props);
      this.rocketRef = React.createRef();
      this.fireRef = React.createRef();
      this.cloudRef = React.createRef();
      this.starsRef = React.createRef();
      this.mountainsRef = React.createRef();
   }

   componentDidMount() {
      document.addEventListener("scroll", () => {
         if(this.rocketRef.current) {
            const rocket = this.rocketRef.current;
            rocket.style.top = window.scrollY * -0.7 + "px";
            const fire = this.fireRef.current;
            fire.style.top = window.scrollY * -0.7 + "px";
            //clouds
            const clouds = this.cloudRef.current;
            clouds.style.top = window.scrollY * 0.10 + "px";
            // stars
            const stars = this.starsRef.current;
            stars.style.top = window.scrollY * -0.12 + "px";
            //mountains
            const mountains = this.mountainsRef.current;
            mountains.style.right = window.scrollY * -0.35 + "px";
         }
      })
   }

   render() {
      return (
        <section>
           {/*2*/}
           <img id="rocket" src={rocket} ref={this.rocketRef} alt=""/>
           {/*3*/}
           <img id="clouds" src={clouds} ref={this.cloudRef} alt=""/>
           {/*4*/}
           <img id="fire" src={fire} ref={this.fireRef} alt=""/>
           {/*5*/}
           {/*text*/}
           <div id="text">
              <h2 className="code-text" data-aos="fade-in">Hello!</h2>
              <p data-aos-offset="100" data-aos-delay="1200" data-aos="fade-right">Looking for someone with good eye for details?</p>
           </div>

           <img id="moon" src={moon} alt=""/>
           {/*6*/}
           <img id="mountains" src={mountains} ref={this.mountainsRef} alt=""/>
           {/*7*/}
           <img id="stars" ref={this.starsRef} src={stars} alt=""/>
           {/*8*/}
        </section>
      )
   }
}

const Home = () => {
   return (
      <>
         <div className="heroContainer">
            <HeroRocket/>
         </div>
         <main>
            <BigEntry/>
            <Container fluid className="contentWrapper">
               <Portfolio />
            </Container>
            <Hero />
         </main>
      </>
   );
};

export default Home;